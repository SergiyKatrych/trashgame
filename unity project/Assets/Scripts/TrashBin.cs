﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TrashBin : MonoBehaviour
{

	[SerializeField] private IDs.TrashType _type;

	public IDs.TrashType TrashType{ get { return _type; } }

	[SerializeField] private Animation _animation;
	public Animation CurrentAnimation{ get { return _animation; } }

	private Action onReadyCallback;

	public void MoveIn(Transform _foodTargetPos, Action onFinishedCallback = null)
	{
		
		onReadyCallback = onFinishedCallback;
	}

	private Action onBinOpened;
	public void OpenBin(Action onCompleteCallback = null)
	{
		onBinOpened = onCompleteCallback;
		_animation.Play(IDs.AnimationNames.Open);
	}

	private Action onBinClosed;
	public void CloseBin(Action onCompleteCallback = null)
	{
		onBinClosed = onCompleteCallback;
		_animation.Play(IDs.AnimationNames.Close);
	}

	public void OnOpened()
	{
		if (onBinOpened != null)
		{
			onBinOpened();
			onBinOpened = null;
		}
	}

	public void OnClosed()
	{
		if (onBinClosed != null)
		{
			onBinClosed();
			onBinClosed = null;
		}
	}
}
