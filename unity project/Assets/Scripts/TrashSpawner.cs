﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashSpawner : MonoBehaviour
{
	
	[SerializeField] private List<TrashComponent> _foodTrashPrefabs;
	[SerializeField] private List<TrashComponent> _glassTrashPrefabs;
	[SerializeField] private List<TrashComponent> _paperTrashPrefabs;
	[SerializeField] private List<TrashComponent> _plasticTrashPrefabs;

	[SerializeField] private float spawnYOffset = 3;

	private TrashBin _foodBin, _glassBin, _paperBin, _plasticBin;
	private GameManager _gameManager;

	public void Init(GameManager gameManager)
	{
		_gameManager = gameManager;
	}

	public void SetBins(TrashBin foodBin, TrashBin glassBin, TrashBin paperBin, TrashBin plasticBin)
	{
		_foodBin = foodBin;
		_glassBin = glassBin;
		_paperBin = paperBin;
		_plasticBin = plasticBin;
	}

	public void Spawn(int foodTrashCount, int glassTrashCount, int papperTrashCount, int plasticTrashCount)
	{
		SpawnSpecificTrash(IDs.TrashType.Food, foodTrashCount, _foodBin);
		SpawnSpecificTrash(IDs.TrashType.Glass, glassTrashCount, _glassBin);
		SpawnSpecificTrash(IDs.TrashType.Paper, papperTrashCount, _paperBin);
		SpawnSpecificTrash(IDs.TrashType.Plastic, plasticTrashCount, _plasticBin);
	}

	private void SpawnSpecificTrash(IDs.TrashType type, int count, TrashBin bin)
	{
		List<TrashComponent> targetList = GetSpecificList(type);
		if (targetList.Count == 0)
		{
			Debug.LogError(type + " list is empty");
			return;
		}
		else
		{
			for (int i = 0; i < count; i++)
			{
				TrashComponent trashElement = Instantiate<TrashComponent>(GetRandomTrash(targetList), transform);
				trashElement.Init(bin, _gameManager);
				trashElement.gameObject.SetActive(true);
			}
		}
	}

	private TrashComponent GetRandomTrash(List<TrashComponent> list)
	{
		return list[Random.Range(0, list.Count)];

	}

	private List<TrashComponent> GetSpecificList(IDs.TrashType type)
	{
		switch (type)
		{
			case IDs.TrashType.Food:
				return _foodTrashPrefabs;
			case IDs.TrashType.Glass:
				return _glassTrashPrefabs;
			case IDs.TrashType.Paper:
				return _paperTrashPrefabs;
			case IDs.TrashType.Plastic:
			default:
				return _plasticTrashPrefabs;
		}
	}
}
