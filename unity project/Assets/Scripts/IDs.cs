﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IDs
{

	public enum GameState
	{
		Opening = 0,
		Gameplay = 1,
		Finish = 2
	}

	public enum TrashType
	{
		Food = 0,
		Glass = 1,
		Paper = 2,
		Plastic = 3
	}

	public struct AnimationNames
	{
		public static string Rolling = "BinRolling";
		public static string Open = "BinOpen";
		public static string Close = "BinClose";
	}
}
