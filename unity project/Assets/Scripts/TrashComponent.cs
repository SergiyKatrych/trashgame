﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Rigidbody))]
public class TrashComponent : MonoBehaviour
{
	[SerializeField] private IDs.TrashType _trashType;

	public IDs.TrashType TrashType { get { return _trashType; } }


	private Rigidbody _rigidBody;
	private TrashBin _targetBin;
	private GameManager _gameManager;


	public void Init(TrashBin bin, GameManager gameManager)
	{
		_targetBin = bin;
		_gameManager = gameManager;
		if (_rigidBody == null)
		{
			_rigidBody = GetComponent<Rigidbody>();
		}
	}

	public void OnPointerDown()
	{
		_targetBin.OpenBin();
		_rigidBody.useGravity = false;
		_rigidBody.mass = 10;
	}

	public void OnPointerHolding(Vector2 pos)
	{
		_rigidBody.MovePosition(pos);
	}

	public void OnPointerUp(Vector2 pos, bool isAboveTheTrash)
	{
		_rigidBody.MovePosition(pos);

		if (!isAboveTheTrash)
		{
			_rigidBody.mass = 1;
			_rigidBody.useGravity = true;
			_targetBin.CloseBin();
		}
		else
		{
			_rigidBody.detectCollisions = false;
			transform.DOMove(_targetBin.transform.position, 0.5f).OnComplete(() =>
				{
					
					Destroy(gameObject);
					_targetBin.CloseBin();


					_gameManager.OnTrashInBin();
				});
		}
	}
}
