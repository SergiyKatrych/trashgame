﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class BinsController : MonoBehaviour
{

	[SerializeField] private float _appearanceDuration = 3;
	[SerializeField] private float _rotationDuration = 0.1f;
	[Header("Bins")]
	[SerializeField] private TrashBin _foodBin;
	[SerializeField] private TrashBin _glassBin, _paperBin, _plasticBin;

	[Header("Start Positions")]
	[SerializeField] private Transform _foodStartPos;
	[SerializeField] private Transform _glassStartPos, _paperStartPos, _plasticStartPos;

	[Header("Target Positions")]
	[SerializeField] private Transform _foodTargetPos;
	[SerializeField] private Transform _glassTargetPos, _paperTargetPos, _plasticTargetPos;



	private Action onCompletedIntroCallback;

	public void Init(Action onIntroCompleted)
	{
		_foodBin.transform.position = _foodStartPos.position;
		_glassBin.transform.position = _glassStartPos.position;
		_paperBin.transform.position = _paperStartPos.position;
		_plasticBin.transform.position = _plasticStartPos.position;

		_foodBin.transform.rotation = Quaternion.Euler(Vector3.up * 90);
		_glassBin.transform.rotation = Quaternion.Euler(Vector3.up * 90);
		_paperBin.transform.rotation = Quaternion.Euler(Vector3.up * 90);
		_plasticBin.transform.rotation = Quaternion.Euler(Vector3.up * 90);

		onCompletedIntroCallback = onIntroCompleted;

		MoveIn();
	}

	private void MoveIn()
	{
		_foodBin.transform.DOMove(_foodTargetPos.position, _appearanceDuration);
		_glassBin.transform.DOMove(_glassTargetPos.position, _appearanceDuration);
		_paperBin.transform.DOMove(_paperTargetPos.position, _appearanceDuration);
		_plasticBin.transform.DOMove(_plasticTargetPos.position, _appearanceDuration).OnComplete(OnMovedIn);
	}

	private void OnMovedIn()
	{
		_foodBin.transform.DORotate(_foodTargetPos.rotation.eulerAngles, _rotationDuration);
		_glassBin.transform.DORotate(_glassTargetPos.rotation.eulerAngles, _rotationDuration);
		_paperBin.transform.DORotate(_paperTargetPos.rotation.eulerAngles, _rotationDuration);
		_plasticBin.transform.DORotate(_plasticTargetPos.rotation.eulerAngles, _rotationDuration).OnComplete(OnRotated);
	}

	private void OnRotated()
	{
		_foodBin.OpenBin(() =>
			{
				_foodBin.CloseBin();
			});
		
		_glassBin.OpenBin(() =>
			{
				_glassBin.CloseBin();
			});
		
		_paperBin.OpenBin(() =>
			{
				_paperBin.CloseBin();
			});
		
		_plasticBin.OpenBin(() =>
			{
				_plasticBin.CloseBin(() =>
					{
						onCompletedIntroCallback();
					});
			});
	}
}
