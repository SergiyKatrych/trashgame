﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
	#region Properties
	[Tooltip("Trash counts")]
	[SerializeField] private int _foodTrashCount = 3;
	[SerializeField] private int _glassTrashCount = 3;
	[SerializeField] private int _paperTrashCount = 3;
	[SerializeField] private int _plasticTrashCount = 3;

	private int trashLeftCount = 0;

	[Space]
	[SerializeField] private BinsController _binController;
	[SerializeField] private TrashSpawner _trashSpawner;
	[SerializeField] private TrashBin _foodBin, _glassBin, _paperBin, _plasticBin;

	[SerializeField] private ParticleSystem starsParticles;
	#endregion

	private IDs.GameState previousGameState = IDs.GameState.Finish;
	private IDs.GameState currentGameState = IDs.GameState.Opening;


	// Use this for initialization
	void Start()
	{
		if (_trashSpawner == null)
		{
			_trashSpawner = FindObjectOfType<TrashSpawner>();
		}

		SetCurrentGameState(IDs.GameState.Opening);
	}
	
	// Update is called once per frame
	void Update()
	{


	}
		

	#region GameState changing
	private void SetCurrentGameState(IDs.GameState state)
	{
		currentGameState = state;
		switch (state)
		{
			case IDs.GameState.Opening:
				OnOpeningStarted();
				break;
			case IDs.GameState.Gameplay:
				OnGameplayStarted();
				break;

			case IDs.GameState.Finish:
			default:
				OnFinisStarted();
				break;
		}
	}


	private void OnOpeningStarted()
	{
		_binController.Init(() =>
			{
				SetCurrentGameState(IDs.GameState.Gameplay);
			});
	}

	private void OnGameplayStarted()
	{
		_trashSpawner.Init(this);
		_trashSpawner.SetBins(_foodBin, _glassBin, _paperBin, _plasticBin);
		_trashSpawner.Spawn(_foodTrashCount, _glassTrashCount, _paperTrashCount, _plasticTrashCount);
		trashLeftCount = _foodTrashCount + _glassTrashCount + _paperTrashCount + _plasticTrashCount;
	}

	private void OnFinisStarted()
	{
		starsParticles.Play();
		DOTween.Sequence().OnComplete(Start).SetDelay(3).Play();
	}
	#endregion

	public void OnTrashInBin()
	{
		trashLeftCount--;
		if (trashLeftCount <= 0)
		{
			SetCurrentGameState(IDs.GameState.Finish);
		}
	}

}
