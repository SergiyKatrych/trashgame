﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomInputController : MonoBehaviour
{
	[SerializeField] private int _trashBin = 10;
	[SerializeField] private int _trashLayer = 11;
	[SerializeField] private int _trashWall = 12;


	private bool _isPressed, _isHolding, _IsReleased = false;

	private TrashComponent currentlyDragginTrash;

	// Update is called once per frame
	void Update()
	{
		UpdateCurrentState();
		if (IsPressed() || IsHolding() || IsReleased())
		{
			Vector3 touchPos;

			#if UNITY_EDITOR
			touchPos = Input.mousePosition;
			#else
			touchPos = Input.GetTouch(0).position;
			#endif

			ProcessInput(touchPos);
		}
	}

	private void UpdateCurrentState()
	{
		_isPressed = IsPressed();

		_isHolding = IsHolding();

		_IsReleased = IsReleased();
	}

	private bool IsPressed()
	{
		#if UNITY_EDITOR
		return Input.GetMouseButtonDown(0);
		#else
		return (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began);
		#endif
	}

	private bool IsHolding()
	{
		#if UNITY_EDITOR
		return Input.GetMouseButton(0);
		#else
		return (Input.touchCount > 0 &&(Input.GetTouch (0).phase == TouchPhase.Stationary || Input.GetTouch (0).phase == TouchPhase.Moved));
		#endif
	}

	private bool IsReleased()
	{
		#if UNITY_EDITOR
		return Input.GetMouseButtonUp(0);
		#else
		return (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Ended);
		#endif
	}


	private void ProcessInput(Vector2 touchPos)
	{
		Ray ray = Camera.main.ScreenPointToRay(touchPos);
		RaycastHit[] hits = Physics.RaycastAll(ray, 30);

		Vector3 targetPosOfTrash = Vector3.zero;

		bool didHitBin = false;

		foreach (var item in hits)
		{
			if (item.collider.gameObject.layer == _trashLayer)
			{
				if (currentlyDragginTrash == null)
				{
					currentlyDragginTrash = item.collider.GetComponent<TrashComponent>();
				}
			}

			if (currentlyDragginTrash != null)
			{
				if (item.collider.gameObject.layer == _trashWall)
				{
					targetPosOfTrash = item.point;
				}
				if (item.collider.gameObject.layer == _trashBin)
				{
					if (item.collider.transform.GetComponentInParent<TrashBin>().TrashType == currentlyDragginTrash.TrashType)
					{
						didHitBin = true;
					}
				}
			}
		}

		if (currentlyDragginTrash != null)
		{
			if (_isPressed)
			{
				currentlyDragginTrash.OnPointerDown();
			}
			else if (_isHolding)
			{
				currentlyDragginTrash.OnPointerHolding(targetPosOfTrash);
			}
			else if (_IsReleased)
			{
				currentlyDragginTrash.OnPointerUp(targetPosOfTrash, didHitBin);
				currentlyDragginTrash = null;
			}
		}

	}
}
